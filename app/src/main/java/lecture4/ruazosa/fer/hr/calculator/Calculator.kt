package lecture4.ruazosa.fer.hr.calculator

import java.util.*

/**
 * Created by dejannovak on 24/03/2018.
 */
object Calculator {

    var result: Double = 0.0
    private set

    var expression: MutableList<String> = mutableListOf()
    private set

    fun reset() {
        result = 0.0
        expression = mutableListOf()
    }

    fun addNumber(number: String) {
        try {
            val num = number.toDouble()
        } catch (e: NumberFormatException) {
            throw Exception("Not valid number")
        }

        if (expression.count()%2 == 0) {
            expression.add(number)
        }
        else {
            throw Exception("Not a valid order of numbers in expression")
        }
    }

    fun addOperator(operator: String) {
        if (expression.count()%2 != 1)  {
            throw Exception("Not a valid order of operator in expression")
        }
        when (operator) {
            "+" -> expression.add(operator)
            "-" -> expression.add(operator)
            "X" -> expression.add(operator)
            "%" -> expression.add(operator)
            else -> {
                throw Exception("Not a valid operator")
            }
        }
    }

    fun evaluate() {

        if (expression.count() % 2 == 0) {
            throw Exception("Not a valid expression")
        }
        expression = shuntingYard(expression)
        result = postfixEvaluation(expression)
    }

    fun shuntingYard(expression:MutableList<String>) : MutableList<String>{
        var stack: Stack<String> = Stack()
        var quee : MutableList<String> = mutableListOf()

        for(i in 0..expression.count() - 1 step 1){
            if(isOperator(expression[i])){
                if(expression[i].equals("+")) {
                    if(!stack.isEmpty())
                        if (stack.peek().equals("*") || stack.peek().equals("%"))
                             quee.add(stack.pop())
                    else
                        stack.push(expression[i])
                }
                else if(expression[i].equals("-")){
                    if(!stack.isEmpty())
                        if (stack.peek().equals("*") || stack.peek().equals("%"))
                            quee.add(stack.pop())
                    else
                        stack.push(expression[i])
                }
                else
                    stack.push(expression[i])

            }
            else if(isDigit(expression[i])){
                quee.add(expression[i])
            }
        }
        while(!stack.isEmpty()){
            quee.add(stack.pop())
        }
        return quee
    }

    fun isOperator(sign : String) : Boolean{
        if(sign.equals("+") || sign.equals("-") || sign.equals("X") || sign.equals("%"))
            return true;
        return false
    }

    fun isDigit(sign : String) : Boolean{
        try{
            sign.toDouble()
        }catch (e : NumberFormatException){
            return false
        }
        return true
    }

    fun postfixEvaluation(expression: MutableList<String>) : Double{
        var lisOfOperands : Stack<String> = Stack()
        var result : Double
        for(i in 0..expression.count() - 1 step 1){
            if(isDigit(expression[i]))
                lisOfOperands.add(expression[i])
            else if(isOperator(expression[i])) {
                if(expression[i].equals("X")) {
                    result = lisOfOperands.pop().toDouble() * lisOfOperands.pop().toDouble()
                }
                else if(expression[i].equals("%")){
                    result = lisOfOperands.pop().toDouble() / lisOfOperands.pop().toDouble()
                }
                else if(expression[i].equals("+")){
                    result = lisOfOperands.pop().toDouble() + lisOfOperands.pop().toDouble()
                }
                else{
                    result = lisOfOperands.pop().toDouble() - lisOfOperands.pop().toDouble()
                }
                lisOfOperands.push(result.toString())
            }
        }
        return lisOfOperands.pop().toDouble()
    }

}